<?php


namespace Huangfsd\OssLog;

use Huangfsd\OssLog\Core\Facade;

/**
 * Class Command
 *
 * @method static array pagination(array $args = [], float $page = 1, float $size = 10, string $order = 'id', string $sort = 'ASC')
 * @method static array details(float $id)
 * @method static void cron()
 *
 * @package Huangfsd\OssLog
 *
 * @see \Huangfsd\OssLog\Support\Command
 */
class Command extends Facade
{
    public static function getFacadeAccessor() {
        return 'command';
    }
}