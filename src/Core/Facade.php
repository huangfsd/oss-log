<?php

namespace Huangfsd\OssLog\Core;

class Facade {

    public static function __callStatic($func, $parameters) {
        $instanceName = static::getFacadeAccessor();
        $instance = FacadeContainer::getInstance($instanceName);
        return $instance->$func(...$parameters);
    }

    public static function getFacadeAccessor() {
        throw new \RuntimeException("Facade does not implement getFacadeAccessor method.");
    }

}