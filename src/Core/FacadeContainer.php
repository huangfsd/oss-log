<?php

namespace Huangfsd\OssLog\Core;

class FacadeContainer {
    private static $container;
    private function __construct($args){}

    public static function init($args) {
        foreach ($args as $key => $value) {
            self::$container[$key] = new $value();
        }
    }

    public static function getInstance($name) {
        return self::$container[$name];
    }
}