<?php


namespace Huangfsd\OssLog;

use Dotenv\Dotenv;

class Config
{
    const BACK = '..'.DIRECTORY_SEPARATOR;
    const DIR = __DIR__.DIRECTORY_SEPARATOR;
    const ROOT_PATH = self::DIR.self::BACK.self::BACK.self::BACK.self::BACK;

    const INSTALL_FILE = self::DIR.self::BACK.'installed';
    const TEMPLATE = self::DIR.self::BACK.'env.template.php';
    const ENV_FILE = self::ROOT_PATH.'.env';
    const STORAGE = self::DIR.'Storage';

    const OSS_MODE = 0;
    const LOCAL2OSS_MODE = 1;
    const LOCAL_MODE = 2;

    public static $log_mode;

    public static $log_local_path;

    public static $oss_path;
    public static $oss_appkey;
    public static $oss_appsecret;
    public static $oss_endpoint;
    public static $oss_bucket;

    public static $db_connection;
    public static $db_host;
    public static $db_port;
    public static $db_database;
    public static $db_username;
    public static $db_password;
    public static $db_table;


    /**
     * install env
     * @author huangfsd
     */
    private static function install() {
        if (!file_exists(self::INSTALL_FILE)) {
            file_put_contents(self::ENV_FILE, file_get_contents(self::TEMPLATE), FILE_APPEND);
            file_put_contents(self::INSTALL_FILE, '');
        }
    }


    /**
     * install and set oss config
     * @author huangfsd
     */
    public static function init() {
        self::install();
        $dotenv = Dotenv::createMutable(self::ROOT_PATH);
        $dotenv->safeLoad();

        self::$log_mode       = getenv('LOG_MODE');

        self::$log_local_path = getenv('LOG_LOCAL_PATH');

        self::$oss_path       = getenv('ALI_OSS_LOG_PATH');
        self::$oss_bucket     = getenv('ALI_OSS_LOG_BUCKET');
        self::$oss_appkey     = getenv('ALI_OSS_LOG_APPKEY');
        self::$oss_appsecret  = getenv('ALI_OSS_LOG_APPSECRET');
        self::$oss_endpoint   = getenv('ALI_OSS_LOG_ENDPOINT');

        self::$db_connection  = getenv('LOG_DB_CONNECTION');
        self::$db_host        = getenv('LOG_DB_HOST');
        self::$db_port        = getenv('LOG_DB_PORT');
        self::$db_database    = getenv('LOG_DB_DATABASE');
        self::$db_username    = getenv('LOG_DB_USERNAME');
        self::$db_password    = getenv('LOG_DB_PASSWORD');
        self::$db_table       = getenv('LOG_DB_TABLE');
    }
}