<?php


namespace Huangfsd\OssLog\Support;

use Exception;
use ReflectionClass;
use ReflectionException;

/**
 * Class Log
 * @method Log error(string $description, array $loggingArgs = [], float $level = 128)
 * @method Log warning(string $description, array $loggingArgs = [], float $level = 128)
 * @method Log info(string $description, array $loggingArgs = [], float $level = 128)
 * @method Log debug(string $description, array $loggingArgs = [], float $level = 128)
 * @package Huangfsd\OssLog\Support
 */
class Log
{
    protected $type;
    protected $level = 128;
    protected $username = '';
    protected $userID = 0;
    protected $debug_backtrace = [];


    /**
     * reset
     * @author huangfsd
     */
    private function reset() {
        $this->level = 128;
        $this->username = '';
        $this->userID = 0;
        $this->debug_backtrace = [];
    }


    /**
     * initial data
     * @param string $username
     * @param int $userID
     * @return $this
     * @author huangfsd
     */
    public function init($username = '', $userID = 0) {
        $this->username = strval($username);
        $this->userID = floatval($userID);
        return $this;
    }


    /**
     * magic function __call
     * @param $name
     * @param $arguments
     * @return $this
     * @throws ReflectionException
     * @author huangfsd
     */
    public function __call($name, $arguments) {
        $command = $this->executeCommand($this, $name, $arguments);
        $this->reset();
        return $command;
    }


    /**
     * execute class
     * @param $class
     * @param $name
     * @param $arguments
     * @return mixed
     * @throws ReflectionException
     * @throws Exception
     * @author huangfsd
     */
    public function executeCommand($class, $name, $arguments) {
        $this->debug_backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
        // remove the last trace since it would be the entry script, not very useful
        array_pop($this->debug_backtrace);
        array_pop($this->debug_backtrace);
        $property = [
            'type'  => $name,
            'level' => $class->level,
            'username' => $class->username,
            'userID' => $class->userID,
            'debug_backtrace' => $class->debug_backtrace,
        ];

        $commandName = $this->getCommandClassName($name);
        $command = new $commandName();

        $command->execute($property, $arguments);
        return $command;
    }


    /**
     * get class
     * @param $name
     * @return string
     * @throws ReflectionException
     * @throws Exception
     * @author huangfsd
     */
    private function getCommandClassName($name) {
        $name = mb_convert_case($name[0], MB_CASE_UPPER, 'utf-8') . mb_substr($name, 1, mb_strlen($name));

        $drivername = $this->getDriverName();
        $classname = sprintf('\Huangfsd\OssLog\Driver\%sCommand', ucfirst($name));

        if (class_exists($classname)) {
            return $classname;
        }

        throw new Exception(
            "Command ({$name}) is not available for driver ({$drivername})."
        );
    }


    /**
     * get namespace
     * @return false|string
     * @throws ReflectionException
     * @author huangfsd
     */
    public function getDriverName() {
        $reflect = new ReflectionClass($this);
        $namespace = $reflect->getNamespaceName();
        return substr(strrchr($namespace, "\\"), 1);
    }

}
