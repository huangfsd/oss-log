<?php


namespace Huangfsd\OssLog\Support;


use Huangfsd\OssLog\Config;
use OSS\Core\OssException;
use OSS\OssClient;

class Command
{

    /**
     * get log list
     * @param array $args
     * @param int $page
     * @param int $size
     * @param array $order
     * @return array
     * @author huangfsd
     */
    public function pagination($args = [], $page = 1, $size = 10, $order = ['level'=>'desc','id'=>'desc']) {
        $query = (new Database())->field(['type', 'level', 'userID', 'username', 'description', 'datetime'])
            ->order($order)
            ->limit($page, $size);
        if ($args) $query = $query->where($args);

        return $query->select(Config::$db_table);
    }


    /**
     * get log details
     * @param $id
     * @return array|mixed
     * @throws \OSS\Core\OssException
     * @author huangfsd
     */
    public function details($id) {
        $id = floatval($id);

        if (empty($id)) return [];

        if (!$details = (new Database())->field(['type', 'level', 'userID', 'username', 'description', 'local', 'oss', 'response', 'datetime'])
            ->where(['id' => $id])
            ->select(Config::$db_table)) return [];

        $details = $details[0] ?? [];

        $details['response'] = json_decode($details['response'], true);

        if (!empty($details['oss'])) {
            try {
                $log = (new OssClient(Config::$oss_appkey, Config::$oss_appsecret, Config::$oss_endpoint))
                    ->getObject(Config::$oss_bucket, $details['oss']);
            } catch (OssException $e) {
                $log = [];
            }
        } elseif(!empty($details['local'])) {
            $log = file_get_contents($details['local']);
        } else {
            $log = [];
        }

        if (!empty($log) && function_exists('gzuncompress')) {
            $log = gzuncompress($log);
        }

        $details['log'] = empty($log) ? $log : unserialize($log);

        return $details;
    }


    /**
     * upload to oss by cron
     * @throws OssException
     * @author huangfsd
     */
    public function cron() {
        //扫描文件夹
        $dateDir = scandir(Config::STORAGE);

        $client = new OssClient(Config::$oss_appkey, Config::$oss_appsecret, Config::$oss_endpoint);
        $mysql = new Database();

        foreach ($dateDir ?? [] as $key => $one) {
            if ($one == '..' || $one == '.') continue;

            $minuteDir = scandir(Config::STORAGE.'/'.$one);

            foreach ($minuteDir ?? [] as $index => $item) {
                if ($item == '..' || $item == '.') continue;

                //上传文件，注意对上oss目录结构
                $ossDir = str_replace('\\', '/', Config::$oss_path).'/'.$one.'/'.$item;
                $localDir = str_replace('\\', '/', Config::STORAGE).'/'.$one.'/'.$item;

                try {
                    $response = $client->uploadDir(Config::$oss_bucket, $ossDir, $localDir);
                } catch (OssException $exception) {
                    $response = $exception->getErrorMessage();
                }

                $files = scandir($localDir);

                foreach ($files ?? [] as $file) {
                    if ($file == '..' || $file == '.') continue;
                    $oss = $ossDir.'/'.$file;
                    $local = $localDir.'/'.$file;

                    //存库
                    if ($mysql->where(['local'=> $local])->update(Config::$db_table, ['oss' => $oss])) {
                        unlink($local);
                    }
                }

                //删掉目录
                if (count(scandir($localDir)) == 2)
                    rmdir($localDir);
            }

            //删掉目录
            if (count(scandir(Config::STORAGE.'/'.$one)) == 2)
                rmdir(Config::STORAGE.'/'.$one);
        }
    }

}