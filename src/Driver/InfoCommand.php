<?php


namespace Huangfsd\OssLog\Driver;


class InfoCommand extends AbstractCommand
{
    /**
     * logging info
     * @param $property
     * @param $arguments
     * @return int
     * @author huangfsd
     */
    public function execute($property, $arguments) {
        return $this->log(
            $arguments[0],
            $property['type'],
            isset($arguments[2]) ? floatval($arguments[2]) : $property['level'],
            $property['username'],
            $property['userID'],
            [
                'debug_backtrace' => $property['debug_backtrace'],
                'request' => $_REQUEST,
                'args' => $arguments[1] ?? [],
            ]
        );
    }
}