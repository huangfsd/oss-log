<?php


namespace Huangfsd\OssLog\Driver;

use Huangfsd\OssLog\Config;
use Huangfsd\OssLog\Support\Database;
use OSS\Core\OssException;
use OSS\OssClient;

abstract class AbstractCommand
{

    /**
     * save file and mysql
     * @param $description
     * @param $type
     * @param $level
     * @param $username
     * @param $userID
     * @param $data
     * @return int
     * @author huangfsd
     */
    public function log($description, $type, $level, $username, $userID, $data) {
        $stream = serialize($data);

        if (function_exists('gzcompress')) {
            $stream = gzcompress($stream);
        }

        $local = $oss = $response = '';
        switch (Config::$log_mode) {
            case Config::OSS_MODE:
                try {
                    $client = new OssClient(Config::$oss_appkey, Config::$oss_appsecret, Config::$oss_endpoint);

                    $oss = static::file(Config::$oss_path);
                    $response = $client->putObject(Config::$oss_bucket, $oss, $stream);
                } catch (OssException $e) {
                    $oss = '';
                    $response = [
                        'code' => $e->getCode(),
                        'message' => $e->getErrorMessage(),
                    ];
                }
                break;
            case Config::LOCAL2OSS_MODE:
            case Config::LOCAL_MODE:
                $local = Config::$log_local_path ? static::file(Config::$log_local_path) : static::file(Config::STORAGE);
                file_put_contents($local, $stream);
                break;
            default:
                break;
        }

        $insert = [
            'type' => $type,
            'level' => $level,
            'username' => $username,
            'userID' => $userID,
            'description' => $description,
            'local' => $local,
            'oss' => $oss,
            'response' => json_encode($response),
            'datetime' => date("Y-m-d H:i:s")
        ];

        return (new Database())->insert(Config::$db_table, $insert);
    }


    /**
     * file name
     * @param $path
     * @return string
     * @author huangfsd
     */
    private static function file($path) {
        $path = str_replace('\\', '/', $path);
        $dir = $path.'/'.date('Ymd/Hi');
        if (!is_dir($dir)) mkdir($dir,0777, true);

        return $dir.'/'.uniqid();
    }
}