<?php

namespace Huangfsd\OssLog;

use Huangfsd\OssLog\Core\Facade;

/**
 * Class Log
 * @method static void error(string $description, array $loggingArgs = [], float $level = 128)
 * @method static void warning(string $description, array $loggingArgs = [], float $level = 128)
 * @method static void info(string $description, array $loggingArgs = [], float $level = 128)
 * @method static void debug(string $description, array $loggingArgs = [], float $level = 128)
 * @method static Support\Log init(string $username = '', float $userID = 0)
 *
 * @package Huangfsd\OssLog
 *
 * @see \Huangfsd\OssLog\Support\Log
 */

class Log extends Facade
{
    public static function getFacadeAccessor() {
        return 'log';
    }
}