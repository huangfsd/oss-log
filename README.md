## 日志收集

### 新建数据表

```sql
CREATE TABLE `log_tbl` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`type` CHAR(16) NOT NULL COMMENT '错误类型',
	`level` TINYINT(3) UNSIGNED NOT NULL COMMENT '错误等级',
	`userID` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`username` VARCHAR(64) NOT NULL DEFAULT '',
	`description` VARCHAR(512) NOT NULL COMMENT '错误描述',
	`local` VARCHAR(128) NOT NULL DEFAULT '' COMMENT '本地绝对路径',
	`oss` VARCHAR(128) NOT NULL DEFAULT '' COMMENT 'oss文件',
	`response` VARCHAR(2048) NOT NULL DEFAULT '' COMMENT 'oss 上传响应头',
	`datetime` DATETIME NOT NULL,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `local` (`local`),
	INDEX `typeUsername` (`type`, `username`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
```

### 日志配置说明
```ini
# 处理日志方式
# 0-实时提交，如果访问量大的接口报错，可能存在卡死或者阻塞问题
# 1-先存storage，定时推送oss（需自建任务，调用Command::cron触发推送）
# 2-日志保存storage或指定目录，耗损服务器空间，但无需oss配置
LOG_MODE=1

# 如选择本地保存，可指定目录默认依赖包storage
LOG_LOCAL_PATH=

# 日志数据库
LOG_DB_CONNECTION=mysql
LOG_DB_HOST=127.0.0.1
LOG_DB_PORT=3306
LOG_DB_DATABASE=lumen
LOG_DB_USERNAME=root
LOG_DB_PASSWORD=root
LOG_DB_TABLE=log_tbl

# 日志服务存储oss的路径
ALI_OSS_LOG_PATH=
# 日志服务存储oss的相关配置
ALI_OSS_LOG_BUCKET=
ALI_OSS_LOG_APPKEY=
ALI_OSS_LOG_APPSECRET=
ALI_OSS_LOG_ENDPOINT=
```

### 日志上传
**Huangfsd\OssLog\Log所有日志上传的都调用此类**

init(): 可选。说明用户名及用户id，也可保存具体位置。exp. Log::init('wechat')->info();


##### 几种日志类型

info(): Log::info(错误描述，需要记下来的参数，错误等级);

debug(): Log::debug(错误描述，需要记下来的参数，错误等级);

warning(): Log::warning(错误描述，需要记下来的参数，错误等级);

error(): Log::error(错误描述，需要记下来的参数，错误等级);


### 获取日志
**Huangfsd\OssLog\Command所有日志上传的都调用此类**

Command::pagination() 获取列表分页

Command::details(id) 获取日志详情

Command::cron() 定时推送本地日志到oss