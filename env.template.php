
# 处理日志方式
# 0-实时提交，如果访问量大的接口报错，可能存在卡死或者阻塞问题
# 1-先存storage，定时推送oss（需自建任务，调用Command::cron触发推送）
# 2-日志保存storage或指定目录，耗损服务器空间，但无需oss配置
LOG_MODE=1

# 如选择本地保存，可指定目录默认依赖包storage
LOG_LOCAL_PATH=

# 日志数据库
LOG_DB_CONNECTION=mysql
LOG_DB_HOST=127.0.0.1
LOG_DB_PORT=3306
LOG_DB_DATABASE=lumen
LOG_DB_USERNAME=root
LOG_DB_PASSWORD=root
LOG_DB_TABLE=log_tbl

# 日志服务存储oss的路径
ALI_OSS_LOG_PATH=
# 日志服务存储oss的相关配置
ALI_OSS_LOG_BUCKET=
ALI_OSS_LOG_APPKEY=
ALI_OSS_LOG_APPSECRET=
ALI_OSS_LOG_ENDPOINT=
