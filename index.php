<?php

use Huangfsd\OssLog\Core\FacadeContainer;
use Huangfsd\OssLog\Config;

require_once __DIR__ . '/autoload.php';
require_once __DIR__ . '/vendor/autoload.php';

FacadeContainer::init([
    'log' => Huangfsd\OssLog\Support\Log::class,
    'command' => Huangfsd\OssLog\Support\Command::class,
]);

Config::init();

//var_dump(Config::$log_mode);die;
//var_dump(\Huangfsd\OssLog\Log::init('test')->info('text'));
//var_dump(\Huangfsd\OssLog\Log::info('aaaaa'));
//print_r(\Huangfsd\OssLog\Command::details(14));
//print_r(\Huangfsd\OssLog\Command::pagination(['type'=>'info'],2,3));
//\Huangfsd\OssLog\Command::cron();